EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "RCRWC32A"
Date "2020-08-07"
Rev "32A V1"
Comp "RTUDF"
Comment1 "Robocamp Workshop Clock"
Comment2 "Raivis Deksnis 2020"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:+3.3V #PWR04
U 1 1 5F2D5D45
P 2525 2600
F 0 "#PWR04" H 2525 2450 50  0001 C CNN
F 1 "+3.3V" H 2540 2773 50  0000 C CNN
F 2 "" H 2525 2600 50  0001 C CNN
F 3 "" H 2525 2600 50  0001 C CNN
	1    2525 2600
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D1
U 1 1 5F2DDBBD
P 9450 3000
F 0 "D1" H 9443 2745 50  0000 C CNN
F 1 "LED" H 9443 2836 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm_Clear" H 9450 3000 50  0001 C CNN
F 3 "~" H 9450 3000 50  0001 C CNN
F 4 "ADD in excel" H 9450 3000 50  0001 C CNN "Farnell code"
	1    9450 3000
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R1
U 1 1 5F2DEB90
P 9800 3000
F 0 "R1" V 9604 3000 50  0000 C CNN
F 1 "100R" V 9695 3000 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 9800 3000 50  0001 C CNN
F 3 "~" H 9800 3000 50  0001 C CNN
F 4 "Add in excel" H 9800 3000 50  0001 C CNN "Farnell code"
	1    9800 3000
	0    1    1    0   
$EndComp
Wire Wire Line
	9600 3000 9700 3000
Wire Wire Line
	9900 3000 10000 3000
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J1
U 1 1 5F2ECA21
P 7025 1400
F 0 "J1" H 7075 1717 50  0000 C CNN
F 1 "Conn_02x03_Odd_Even" H 7075 1626 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 7025 1400 50  0001 C CNN
F 3 "https://lv.farnell.com/samtec/tsw-102-07-t-d/connector-header-4pos-2row-2-54mm/dp/2984553?st=2.54%20pin" H 7025 1400 50  0001 C CNN
F 4 "2984553" H 7025 1400 50  0001 C CNN "Farnell code"
F 5 "0.17" H 7025 1400 50  0001 C CNN "price"
	1    7025 1400
	1    0    0    -1  
$EndComp
Text Label 7325 1400 0    50   ~ 0
MOSI
Text Label 6600 1300 0    50   ~ 0
MISO
Text Label 6600 1400 0    50   ~ 0
SCK
Wire Wire Line
	7325 1400 7500 1400
Wire Wire Line
	7325 1500 7500 1500
Wire Wire Line
	6825 1400 6600 1400
Wire Wire Line
	6825 1300 6600 1300
Text Label 6600 1500 0    50   ~ 0
RST
$Comp
L power:GND #PWR014
U 1 1 5F2F1F33
P 7500 1600
F 0 "#PWR014" H 7500 1350 50  0001 C CNN
F 1 "GND" H 7505 1427 50  0000 C CNN
F 2 "" H 7500 1600 50  0001 C CNN
F 3 "" H 7500 1600 50  0001 C CNN
	1    7500 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 1500 7500 1600
Wire Wire Line
	9000 3000 9300 3000
Text Label 9100 3000 0    50   ~ 0
LED1
Wire Wire Line
	9000 3375 9300 3375
Text Label 9100 3375 0    50   ~ 0
LED2
Wire Wire Line
	9000 3750 9300 3750
Text Label 9100 3750 0    50   ~ 0
LED3
Wire Wire Line
	9000 4100 9300 4100
Text Label 9100 4100 0    50   ~ 0
LED4
Wire Wire Line
	9000 4450 9300 4450
Text Label 9100 4450 0    50   ~ 0
LED5
Wire Wire Line
	9000 4800 9300 4800
Text Label 9100 4800 0    50   ~ 0
LED6
Wire Wire Line
	9000 5150 9300 5150
Text Label 9100 5150 0    50   ~ 0
LED7
Wire Wire Line
	9000 5500 9300 5500
Text Label 9100 5500 0    50   ~ 0
LED8
$Comp
L Device:LED D2
U 1 1 5F303F44
P 9450 3375
F 0 "D2" H 9443 3120 50  0000 C CNN
F 1 "LED" H 9443 3211 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm_Clear" H 9450 3375 50  0001 C CNN
F 3 "~" H 9450 3375 50  0001 C CNN
F 4 "ADD in excel" H 9450 3375 50  0001 C CNN "Farnell code"
	1    9450 3375
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R2
U 1 1 5F303F4A
P 9800 3375
F 0 "R2" V 9604 3375 50  0000 C CNN
F 1 "100R" V 9695 3375 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 9800 3375 50  0001 C CNN
F 3 "~" H 9800 3375 50  0001 C CNN
F 4 "Add in excel" H 9800 3375 50  0001 C CNN "Farnell code"
	1    9800 3375
	0    1    1    0   
$EndComp
Wire Wire Line
	9600 3375 9700 3375
Wire Wire Line
	9900 3375 10000 3375
$Comp
L Device:LED D3
U 1 1 5F3053BB
P 9450 3750
F 0 "D3" H 9443 3495 50  0000 C CNN
F 1 "LED" H 9443 3586 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm_Clear" H 9450 3750 50  0001 C CNN
F 3 "~" H 9450 3750 50  0001 C CNN
F 4 "ADD in excel" H 9450 3750 50  0001 C CNN "Farnell code"
	1    9450 3750
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R3
U 1 1 5F3053C1
P 9800 3750
F 0 "R3" V 9604 3750 50  0000 C CNN
F 1 "100R" V 9695 3750 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 9800 3750 50  0001 C CNN
F 3 "~" H 9800 3750 50  0001 C CNN
F 4 "Add in excel" H 9800 3750 50  0001 C CNN "Farnell code"
	1    9800 3750
	0    1    1    0   
$EndComp
Wire Wire Line
	9600 3750 9700 3750
Wire Wire Line
	9900 3750 10000 3750
$Comp
L Device:LED D4
U 1 1 5F3067BB
P 9450 4100
F 0 "D4" H 9443 3845 50  0000 C CNN
F 1 "LED" H 9443 3936 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm_Clear" H 9450 4100 50  0001 C CNN
F 3 "~" H 9450 4100 50  0001 C CNN
F 4 "ADD in excel" H 9450 4100 50  0001 C CNN "Farnell code"
	1    9450 4100
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R4
U 1 1 5F3067C1
P 9800 4100
F 0 "R4" V 9604 4100 50  0000 C CNN
F 1 "100R" V 9695 4100 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 9800 4100 50  0001 C CNN
F 3 "~" H 9800 4100 50  0001 C CNN
F 4 "Add in excel" H 9800 4100 50  0001 C CNN "Farnell code"
	1    9800 4100
	0    1    1    0   
$EndComp
Wire Wire Line
	9600 4100 9700 4100
Wire Wire Line
	9900 4100 10000 4100
$Comp
L Device:LED D5
U 1 1 5F307B23
P 9450 4450
F 0 "D5" H 9443 4195 50  0000 C CNN
F 1 "LED" H 9443 4286 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm_Clear" H 9450 4450 50  0001 C CNN
F 3 "~" H 9450 4450 50  0001 C CNN
F 4 "ADD in excel" H 9450 4450 50  0001 C CNN "Farnell code"
	1    9450 4450
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R5
U 1 1 5F307B29
P 9800 4450
F 0 "R5" V 9604 4450 50  0000 C CNN
F 1 "100R" V 9695 4450 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 9800 4450 50  0001 C CNN
F 3 "~" H 9800 4450 50  0001 C CNN
F 4 "Add in excel" H 9800 4450 50  0001 C CNN "Farnell code"
	1    9800 4450
	0    1    1    0   
$EndComp
Wire Wire Line
	9600 4450 9700 4450
Wire Wire Line
	9900 4450 10000 4450
$Comp
L Device:LED D6
U 1 1 5F309352
P 9450 4800
F 0 "D6" H 9443 4545 50  0000 C CNN
F 1 "LED" H 9443 4636 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm_Clear" H 9450 4800 50  0001 C CNN
F 3 "~" H 9450 4800 50  0001 C CNN
F 4 "ADD in excel" H 9450 4800 50  0001 C CNN "Farnell code"
	1    9450 4800
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R6
U 1 1 5F309358
P 9800 4800
F 0 "R6" V 9604 4800 50  0000 C CNN
F 1 "100R" V 9695 4800 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 9800 4800 50  0001 C CNN
F 3 "~" H 9800 4800 50  0001 C CNN
F 4 "Add in excel" H 9800 4800 50  0001 C CNN "Farnell code"
	1    9800 4800
	0    1    1    0   
$EndComp
Wire Wire Line
	9600 4800 9700 4800
Wire Wire Line
	9900 4800 10000 4800
$Comp
L Device:LED D7
U 1 1 5F30C6CE
P 9450 5150
F 0 "D7" H 9443 4895 50  0000 C CNN
F 1 "LED" H 9443 4986 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm_Clear" H 9450 5150 50  0001 C CNN
F 3 "~" H 9450 5150 50  0001 C CNN
F 4 "ADD in excel" H 9450 5150 50  0001 C CNN "Farnell code"
	1    9450 5150
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R7
U 1 1 5F30C6D4
P 9800 5150
F 0 "R7" V 9604 5150 50  0000 C CNN
F 1 "100R" V 9695 5150 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 9800 5150 50  0001 C CNN
F 3 "~" H 9800 5150 50  0001 C CNN
F 4 "Add in excel" H 9800 5150 50  0001 C CNN "Farnell code"
	1    9800 5150
	0    1    1    0   
$EndComp
Wire Wire Line
	9600 5150 9700 5150
Wire Wire Line
	9900 5150 10000 5150
$Comp
L Device:LED D8
U 1 1 5F30E3C0
P 9450 5500
F 0 "D8" H 9443 5245 50  0000 C CNN
F 1 "LED" H 9443 5336 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm_Clear" H 9450 5500 50  0001 C CNN
F 3 "~" H 9450 5500 50  0001 C CNN
F 4 "ADD in excel" H 9450 5500 50  0001 C CNN "Farnell code"
	1    9450 5500
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R8
U 1 1 5F30E3C6
P 9800 5500
F 0 "R8" V 9604 5500 50  0000 C CNN
F 1 "100R" V 9695 5500 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 9800 5500 50  0001 C CNN
F 3 "~" H 9800 5500 50  0001 C CNN
F 4 "Add in excel" H 9800 5500 50  0001 C CNN "Farnell code"
	1    9800 5500
	0    1    1    0   
$EndComp
Wire Wire Line
	9600 5500 9700 5500
Wire Wire Line
	9900 5500 10000 5500
Wire Wire Line
	10000 3000 10000 3375
Connection ~ 10000 3375
Wire Wire Line
	10000 3375 10000 3750
Connection ~ 10000 3750
Wire Wire Line
	10000 3750 10000 4100
Connection ~ 10000 4100
Wire Wire Line
	10000 4100 10000 4450
Connection ~ 10000 4450
Wire Wire Line
	10000 4450 10000 4800
Connection ~ 10000 4800
Wire Wire Line
	10000 4800 10000 5150
Connection ~ 10000 5150
Wire Wire Line
	10000 5150 10000 5500
Connection ~ 10000 5500
$Comp
L power:GND #PWR016
U 1 1 5F31AAA3
P 10000 5950
F 0 "#PWR016" H 10000 5700 50  0001 C CNN
F 1 "GND" H 10005 5777 50  0000 C CNN
F 2 "" H 10000 5950 50  0001 C CNN
F 3 "" H 10000 5950 50  0001 C CNN
	1    10000 5950
	1    0    0    -1  
$EndComp
$Comp
L Device:R_POT RV1
U 1 1 5F31C6FB
P 7150 2575
F 0 "RV1" H 7080 2529 50  0000 R CNN
F 1 "R_POT" H 7080 2620 50  0000 R CNN
F 2 "RDsInc:R0141-2" H 7150 2575 50  0001 C CNN
F 3 "https://lv.farnell.com/amphenol-piher-sensorscontrols/pt6kh-103a2020-pm/trimmer-10k-0-1w-1turn/dp/3128562?st=piher%20PT%206" H 7150 2575 50  0001 C CNN
F 4 "3128562" H 7150 2575 50  0001 C CNN "Farnell code"
F 5 "0.78" H 7150 2575 50  0001 C CNN "price"
	1    7150 2575
	-1   0    0    1   
$EndComp
Wire Wire Line
	9025 5850 9325 5850
Text Label 9125 5850 0    50   ~ 0
LED9
$Comp
L Device:LED D9
U 1 1 5F3206CE
P 9475 5850
F 0 "D9" H 9468 5595 50  0000 C CNN
F 1 "LED" H 9468 5686 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm_Clear" H 9475 5850 50  0001 C CNN
F 3 "~" H 9475 5850 50  0001 C CNN
F 4 "ADD in excel" H 9475 5850 50  0001 C CNN "Farnell code"
	1    9475 5850
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R9
U 1 1 5F3206D4
P 9825 5850
F 0 "R9" V 9629 5850 50  0000 C CNN
F 1 "100R" V 9720 5850 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 9825 5850 50  0001 C CNN
F 3 "~" H 9825 5850 50  0001 C CNN
F 4 "Add in excel" H 9825 5850 50  0001 C CNN "Farnell code"
	1    9825 5850
	0    1    1    0   
$EndComp
Wire Wire Line
	9625 5850 9725 5850
Wire Wire Line
	10000 5500 10000 5850
Wire Wire Line
	9925 5850 10000 5850
Connection ~ 10000 5850
Wire Wire Line
	10000 5850 10000 5950
Text Label 6725 2575 0    50   ~ 0
POT1
Wire Wire Line
	6725 2575 7000 2575
$Comp
L power:GND #PWR012
U 1 1 5F32F649
P 7150 2725
F 0 "#PWR012" H 7150 2475 50  0001 C CNN
F 1 "GND" H 7155 2552 50  0000 C CNN
F 2 "" H 7150 2725 50  0001 C CNN
F 3 "" H 7150 2725 50  0001 C CNN
	1    7150 2725
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 5F32FBA1
P 2525 4800
F 0 "#PWR03" H 2525 4550 50  0001 C CNN
F 1 "GND" H 2530 4627 50  0000 C CNN
F 2 "" H 2525 4800 50  0001 C CNN
F 3 "" H 2525 4800 50  0001 C CNN
	1    2525 4800
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR011
U 1 1 5F330031
P 7150 2425
F 0 "#PWR011" H 7150 2275 50  0001 C CNN
F 1 "+3.3V" H 7165 2598 50  0000 C CNN
F 2 "" H 7150 2425 50  0001 C CNN
F 3 "" H 7150 2425 50  0001 C CNN
	1    7150 2425
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW2
U 1 1 5F330E81
P 9425 1650
F 0 "SW2" H 9425 1935 50  0000 C CNN
F 1 "SW_Push" H 9425 1844 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH-12mm" H 9425 1850 50  0001 C CNN
F 3 "https://lv.farnell.com/alcoswitch-te-connectivity/1825910-7/tactile-switch-spst-0-05a-24v/dp/2468761?st=tact%20switch" H 9425 1850 50  0001 C CNN
F 4 "2469761" H 9425 1650 50  0001 C CNN "Farnell code"
F 5 "0.07" H 9425 1650 50  0001 C CNN "price"
	1    9425 1650
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW3
U 1 1 5F332A4D
P 9425 2050
F 0 "SW3" H 9425 2335 50  0000 C CNN
F 1 "SW_Push" H 9425 2244 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH-12mm" H 9425 2250 50  0001 C CNN
F 3 "https://lv.farnell.com/alcoswitch-te-connectivity/1825910-7/tactile-switch-spst-0-05a-24v/dp/2468761?st=tact%20switch" H 9425 2250 50  0001 C CNN
F 4 "2469761" H 9425 2050 50  0001 C CNN "Farnell code"
F 5 "0.07" H 9425 2050 50  0001 C CNN "price"
	1    9425 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	9700 2050 9625 2050
Wire Wire Line
	9625 1650 9700 1650
Wire Wire Line
	9700 1650 9700 2050
Wire Wire Line
	9025 1650 9225 1650
Wire Wire Line
	9225 2050 9025 2050
Text Label 9025 1650 0    50   ~ 0
SW1
Text Label 9025 2050 0    50   ~ 0
SW2
$Comp
L Device:Battery_Cell BT1
U 1 1 5F344F77
P 3075 6350
F 0 "BT1" V 2825 6250 50  0000 C CNN
F 1 "Battery_Cell" V 2925 6275 50  0000 C CNN
F 2 "RDsInc:MP000360_CR2032_holder" V 3075 6410 50  0001 C CNN
F 3 "https://lv.farnell.com/multicomp-pro/mp000360/battery-holder-2032-through-hole/dp/3126599" V 3075 6410 50  0001 C CNN
F 4 "1650693" H 3075 6350 50  0001 C CNN "Farnell code"
F 5 "0.87" H 3075 6350 50  0001 C CNN "price"
	1    3075 6350
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR05
U 1 1 5F346DE2
P 2900 6425
F 0 "#PWR05" H 2900 6175 50  0001 C CNN
F 1 "GND" H 2905 6252 50  0000 C CNN
F 2 "" H 2900 6425 50  0001 C CNN
F 3 "" H 2900 6425 50  0001 C CNN
	1    2900 6425
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 6425 2900 6350
Wire Wire Line
	2900 6350 2975 6350
Wire Wire Line
	4100 6250 4300 6250
Wire Wire Line
	4300 6250 4300 6150
$Comp
L power:+3.3V #PWR06
U 1 1 5F34BD06
P 4300 6150
F 0 "#PWR06" H 4300 6000 50  0001 C CNN
F 1 "+3.3V" H 4315 6323 50  0000 C CNN
F 2 "" H 4300 6150 50  0001 C CNN
F 3 "" H 4300 6150 50  0001 C CNN
	1    4300 6150
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_SPDT SW1
U 1 1 5F34C9C2
P 3900 6250
F 0 "SW1" H 3900 6535 50  0000 C CNN
F 1 "SW_SPDT" H 3900 6444 50  0000 C CNN
F 2 "Button_Switch_THT:SW_CuK_OS102011MA1QN1_SPDT_Angled" H 3900 6250 50  0001 C CNN
F 3 "https://lv.farnell.com/c-k-components/os102011ma1qn1/switch-spdt-0-1a-12v-pcb-r-a/dp/1201431?st=OS102011MA1QN1" H 3900 6250 50  0001 C CNN
F 4 "1201431" H 3900 6250 50  0001 C CNN "Farnell code"
F 5 "0.39" H 3900 6250 50  0001 C CNN "price"
	1    3900 6250
	-1   0    0    1   
$EndComp
Wire Wire Line
	3125 4200 3425 4200
$Comp
L Connector:TestPoint TP1
U 1 1 5F360928
P 3425 4200
F 0 "TP1" V 3379 4388 50  0000 L CNN
F 1 "TestPoint" V 3470 4388 50  0000 L CNN
F 2 "TestPoint:TestPoint_Loop_D2.50mm_Drill1.0mm" H 3625 4200 50  0001 C CNN
F 3 "~" H 3625 4200 50  0001 C CNN
	1    3425 4200
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP2
U 1 1 5F361555
P 3750 4100
F 0 "TP2" V 3704 4288 50  0000 L CNN
F 1 "TestPoint" V 3795 4288 50  0000 L CNN
F 2 "TestPoint:TestPoint_Loop_D2.50mm_Drill1.0mm" H 3950 4100 50  0001 C CNN
F 3 "~" H 3950 4100 50  0001 C CNN
	1    3750 4100
	0    1    1    0   
$EndComp
Wire Wire Line
	3125 4100 3750 4100
$Comp
L Device:C_Small C1
U 1 1 5F2D98F2
P 5275 6650
F 0 "C1" V 5046 6650 50  0000 C CNN
F 1 "100N" V 5137 6650 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 5275 6650 50  0001 C CNN
F 3 "https://lv.farnell.com/avx/12061c104jat2a/cap-0-1-f-100v-5-x7r-1206/dp/2332842?ost=2332842" H 5275 6650 50  0001 C CNN
F 4 "2332842" H 5275 6650 50  0001 C CNN "Farnell code"
F 5 "0.32" H 5275 6650 50  0001 C CNN "price"
	1    5275 6650
	-1   0    0    1   
$EndComp
$Comp
L power:+3.3V #PWR07
U 1 1 5F2DA6D0
P 5275 6550
F 0 "#PWR07" H 5275 6400 50  0001 C CNN
F 1 "+3.3V" H 5290 6723 50  0000 C CNN
F 2 "" H 5275 6550 50  0001 C CNN
F 3 "" H 5275 6550 50  0001 C CNN
	1    5275 6550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR08
U 1 1 5F2DAB6C
P 5275 6750
F 0 "#PWR08" H 5275 6500 50  0001 C CNN
F 1 "GND" H 5280 6577 50  0000 C CNN
F 2 "" H 5275 6750 50  0001 C CNN
F 3 "" H 5275 6750 50  0001 C CNN
	1    5275 6750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 1500 6350 1325
$Comp
L Device:R_Small R10
U 1 1 5F2DDBD9
P 6350 1225
F 0 "R10" V 6154 1225 50  0000 C CNN
F 1 "4.7k" V 6245 1225 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" H 6350 1225 50  0001 C CNN
F 3 "https://lv.farnell.com/multicomp/mcre000045/res-4k7-5-125mw-axial-carbon-film/dp/1700245?st=4.7k" H 6350 1225 50  0001 C CNN
F 4 "1700245" H 6350 1225 50  0001 C CNN "Farnell code"
F 5 "0.02" H 6350 1225 50  0001 C CNN "price"
	1    6350 1225
	-1   0    0    1   
$EndComp
Wire Wire Line
	6350 1500 6825 1500
$Comp
L power:+3.3V #PWR010
U 1 1 5F2E1141
P 6350 1125
F 0 "#PWR010" H 6350 975 50  0001 C CNN
F 1 "+3.3V" H 6365 1298 50  0000 C CNN
F 2 "" H 6350 1125 50  0001 C CNN
F 3 "" H 6350 1125 50  0001 C CNN
	1    6350 1125
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H1
U 1 1 5F364780
P 6500 4575
F 0 "H1" H 6600 4621 50  0000 L CNN
F 1 "MountingHole" H 6600 4530 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 6500 4575 50  0001 C CNN
F 3 "~" H 6500 4575 50  0001 C CNN
	1    6500 4575
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5F367E3C
P 7225 4575
F 0 "H3" H 7325 4621 50  0000 L CNN
F 1 "MountingHole" H 7325 4530 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 7225 4575 50  0001 C CNN
F 3 "~" H 7225 4575 50  0001 C CNN
	1    7225 4575
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5F3681BE
P 6500 4975
F 0 "H2" H 6600 5021 50  0000 L CNN
F 1 "MountingHole" H 6600 4930 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 6500 4975 50  0001 C CNN
F 3 "~" H 6500 4975 50  0001 C CNN
	1    6500 4975
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5F368BA0
P 7225 4975
F 0 "H4" H 7325 5021 50  0000 L CNN
F 1 "MountingHole" H 7325 4930 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 7225 4975 50  0001 C CNN
F 3 "~" H 7225 4975 50  0001 C CNN
	1    7225 4975
	1    0    0    -1  
$EndComp
Wire Wire Line
	7325 1300 7800 1300
Text Label 7575 1300 0    50   ~ 0
3Vfeed
Text Label 3375 6150 0    50   ~ 0
3Vfeed
Wire Wire Line
	3275 6350 3700 6350
Wire Wire Line
	3700 6150 3375 6150
$Comp
L Device:Buzzer BZ1
U 1 1 5F4098AB
P 5325 2900
F 0 "BZ1" H 5477 2929 50  0000 L CNN
F 1 "Buzzer" H 5477 2838 50  0000 L CNN
F 2 "Buzzer_Beeper:Buzzer_TDK_PS1240P02BT_D12.2mm_H6.5mm" V 5300 3000 50  0001 C CNN
F 3 "~" V 5300 3000 50  0001 C CNN
	1    5325 2900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR013
U 1 1 5F40BF0B
P 5175 3100
F 0 "#PWR013" H 5175 2850 50  0001 C CNN
F 1 "GND" H 5180 2927 50  0000 C CNN
F 2 "" H 5175 3100 50  0001 C CNN
F 3 "" H 5175 3100 50  0001 C CNN
	1    5175 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5225 3000 5175 3000
Wire Wire Line
	5175 3000 5175 3100
Wire Wire Line
	9700 2050 9700 2225
Connection ~ 9700 2050
$Comp
L power:GND #PWR09
U 1 1 5F6D7B6C
P 9700 2225
F 0 "#PWR09" H 9700 1975 50  0001 C CNN
F 1 "GND" H 9705 2052 50  0000 C CNN
F 2 "" H 9700 2225 50  0001 C CNN
F 3 "" H 9700 2225 50  0001 C CNN
	1    9700 2225
	1    0    0    -1  
$EndComp
$Comp
L MCU_Microchip_ATtiny:ATtiny4313-PU U1
U 1 1 5F6E53F9
P 2525 3700
F 0 "U1" H 2150 4900 50  0000 C CNN
F 1 "ATtiny4313-PU" H 2125 4800 50  0000 C CNN
F 2 "Package_DIP:DIP-20_W7.62mm" H 2525 3700 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/doc8246.pdf" H 2525 3700 50  0001 C CNN
	1    2525 3700
	1    0    0    -1  
$EndComp
Text Label 3175 3300 0    50   ~ 0
MISO
Text Label 3175 3200 0    50   ~ 0
MOSI
Text Label 3175 3400 0    50   ~ 0
SCK
Text Label 1725 2900 0    50   ~ 0
RST
Wire Wire Line
	1625 2900 1925 2900
Wire Wire Line
	3125 2900 3425 2900
Text Label 3225 2900 0    50   ~ 0
LED1
Wire Wire Line
	3125 3000 3425 3000
Text Label 3225 3000 0    50   ~ 0
LED2
Wire Wire Line
	3125 3100 3425 3100
Text Label 3225 3100 0    50   ~ 0
LED3
Text Label 3525 3200 0    50   ~ 0
LED4
Text Label 3525 3300 0    50   ~ 0
LED5
Text Label 3525 3400 0    50   ~ 0
LED6
Wire Wire Line
	3125 3500 3425 3500
Text Label 3225 3500 0    50   ~ 0
LED7
Wire Wire Line
	3125 3600 3425 3600
Text Label 3225 3600 0    50   ~ 0
LED8
Wire Wire Line
	3125 3800 3425 3800
Text Label 3225 3800 0    50   ~ 0
LED9
Wire Wire Line
	3125 4400 3425 4400
Text Label 3225 4400 0    50   ~ 0
POT1
Text Label 3225 3900 0    50   ~ 0
SW1
Text Label 3225 4000 0    50   ~ 0
SW2
Wire Wire Line
	3125 3400 3725 3400
Wire Wire Line
	3125 3300 3725 3300
Wire Wire Line
	3125 3200 3725 3200
Wire Wire Line
	3125 3900 3425 3900
Wire Wire Line
	3125 4000 3425 4000
Text Label 4875 2800 0    50   ~ 0
buzzer
Wire Wire Line
	4875 2800 5225 2800
Wire Wire Line
	3125 4300 3425 4300
Text Label 3175 4300 0    50   ~ 0
buzzer
$Comp
L Device:C_Small C2
U 1 1 5F740D77
P 5850 6650
F 0 "C2" V 5621 6650 50  0000 C CNN
F 1 "100N" V 5712 6650 50  0000 C CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 5850 6650 50  0001 C CNN
F 3 "https://lv.farnell.com/avx/12061c104jat2a/cap-0-1-f-100v-5-x7r-1206/dp/2332842?ost=2332842" H 5850 6650 50  0001 C CNN
F 4 "2332842" H 5850 6650 50  0001 C CNN "Farnell code"
F 5 "0.32" H 5850 6650 50  0001 C CNN "price"
	1    5850 6650
	-1   0    0    1   
$EndComp
$Comp
L power:+3.3V #PWR01
U 1 1 5F740D7D
P 5850 6550
F 0 "#PWR01" H 5850 6400 50  0001 C CNN
F 1 "+3.3V" H 5865 6723 50  0000 C CNN
F 2 "" H 5850 6550 50  0001 C CNN
F 3 "" H 5850 6550 50  0001 C CNN
	1    5850 6550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5F740D83
P 5850 6750
F 0 "#PWR02" H 5850 6500 50  0001 C CNN
F 1 "GND" H 5855 6577 50  0000 C CNN
F 2 "" H 5850 6750 50  0001 C CNN
F 3 "" H 5850 6750 50  0001 C CNN
	1    5850 6750
	1    0    0    -1  
$EndComp
$EndSCHEMATC
