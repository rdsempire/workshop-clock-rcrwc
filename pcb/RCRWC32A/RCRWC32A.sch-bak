EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "RCRWC32A"
Date "2020-08-07"
Rev "32A V1"
Comp "RTUDF"
Comment1 "Robocamp Workshop Clock"
Comment2 "Raivis Deksnis 2020"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:+3.3V #PWR02
U 1 1 5F2D5597
P 3275 1650
F 0 "#PWR02" H 3275 1500 50  0001 C CNN
F 1 "+3.3V" H 3290 1823 50  0000 C CNN
F 2 "" H 3275 1650 50  0001 C CNN
F 3 "" H 3275 1650 50  0001 C CNN
	1    3275 1650
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR04
U 1 1 5F2D5D45
P 3375 1650
F 0 "#PWR04" H 3375 1500 50  0001 C CNN
F 1 "+3.3V" H 3390 1823 50  0000 C CNN
F 2 "" H 3375 1650 50  0001 C CNN
F 3 "" H 3375 1650 50  0001 C CNN
	1    3375 1650
	1    0    0    -1  
$EndComp
$Comp
L Device:Crystal_Small Y1
U 1 1 5F2D711D
P 4500 2600
F 0 "Y1" V 4454 2688 50  0000 L CNN
F 1 "16M" V 4545 2688 50  0000 L CNN
F 2 "Crystal:Crystal_HC49-4H_Vertical" H 4500 2600 50  0001 C CNN
F 3 "https://lv.farnell.com/multicomp/hc49s-16-30-50-70-30-atf/crystal-hc-49-s-16-0mhz/dp/1667003" H 4500 2600 50  0001 C CNN
F 4 "1667003" H 4500 2600 50  0001 C CNN "Farnell code"
F 5 "0.27" H 4500 2600 50  0001 C CNN "price"
	1    4500 2600
	0    -1   -1   0   
$EndComp
$Comp
L Device:C_Small C3
U 1 1 5F2D8657
P 4725 2700
F 0 "C3" V 4496 2700 50  0000 C CNN
F 1 "30pF" V 4587 2700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4725 2700 50  0001 C CNN
F 3 "https://lv.farnell.com/kemet/c0805c300j5gacauto/cap-30pf-50v-5-c0g-np0-0805/dp/2905258?st=30p" H 4725 2700 50  0001 C CNN
F 4 "2905258" H 4725 2700 50  0001 C CNN "Farnell code"
F 5 "0.24" H 4725 2700 50  0001 C CNN "price"
	1    4725 2700
	0    -1   -1   0   
$EndComp
$Comp
L Device:C_Small C2
U 1 1 5F2D9606
P 4725 2500
F 0 "C2" V 4900 2500 50  0000 C CNN
F 1 "30pF" V 4825 2500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4725 2500 50  0001 C CNN
F 3 "https://lv.farnell.com/kemet/c0805c300j5gacauto/cap-30pf-50v-5-c0g-np0-0805/dp/2905258?st=30p" H 4725 2500 50  0001 C CNN
F 4 "2905258" H 4725 2500 50  0001 C CNN "Farnell code"
F 5 "0.24" H 4725 2500 50  0001 C CNN "price"
	1    4725 2500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4625 2700 4500 2700
Connection ~ 4500 2700
Wire Wire Line
	4500 2700 4125 2700
Wire Wire Line
	4125 2500 4500 2500
Wire Wire Line
	4625 2500 4500 2500
Connection ~ 4500 2500
Wire Wire Line
	4825 2700 4975 2700
Wire Wire Line
	4975 2700 4975 2500
Wire Wire Line
	4825 2500 4975 2500
$Comp
L power:GND #PWR09
U 1 1 5F2DB6BF
P 4975 2825
F 0 "#PWR09" H 4975 2575 50  0001 C CNN
F 1 "GND" H 4980 2652 50  0000 C CNN
F 2 "" H 4975 2825 50  0001 C CNN
F 3 "" H 4975 2825 50  0001 C CNN
	1    4975 2825
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D1
U 1 1 5F2DDBBD
P 9450 3000
F 0 "D1" H 9443 2745 50  0000 C CNN
F 1 "LED" H 9443 2836 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm_Clear" H 9450 3000 50  0001 C CNN
F 3 "~" H 9450 3000 50  0001 C CNN
F 4 "ADD in excel" H 9450 3000 50  0001 C CNN "Farnell code"
	1    9450 3000
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R1
U 1 1 5F2DEB90
P 9800 3000
F 0 "R1" V 9604 3000 50  0000 C CNN
F 1 "100R" V 9695 3000 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 9800 3000 50  0001 C CNN
F 3 "~" H 9800 3000 50  0001 C CNN
F 4 "Add in excel" H 9800 3000 50  0001 C CNN "Farnell code"
	1    9800 3000
	0    1    1    0   
$EndComp
Wire Wire Line
	9600 3000 9700 3000
Wire Wire Line
	9900 3000 10000 3000
$Comp
L MCU_Microchip_ATmega:ATmega328-PU U1
U 1 1 5F2E1402
P 3275 3150
F 0 "U1" H 2631 3196 50  0000 R CNN
F 1 "ATmega328-PU" H 2631 3105 50  0000 R CNN
F 2 "Package_DIP:DIP-28_W7.62mm" H 3275 3150 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/ATmega328_P%20AVR%20MCU%20with%20picoPower%20Technology%20Data%20Sheet%2040001984A.pdf" H 3275 3150 50  0001 C CNN
F 4 "1972087" H 3275 3150 50  0001 C CNN "Farnell code"
F 5 "1.7" H 3275 3150 50  0001 C CNN "price"
	1    3275 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4125 2500 4125 2550
Wire Wire Line
	4125 2550 3875 2550
Wire Wire Line
	3875 2650 4125 2650
Wire Wire Line
	4125 2650 4125 2700
Wire Wire Line
	4975 2700 4975 2825
Connection ~ 4975 2700
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J1
U 1 1 5F2ECA21
P 7025 1400
F 0 "J1" H 7075 1717 50  0000 C CNN
F 1 "Conn_02x03_Odd_Even" H 7075 1626 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 7025 1400 50  0001 C CNN
F 3 "https://lv.farnell.com/samtec/tsw-102-07-t-d/connector-header-4pos-2row-2-54mm/dp/2984553?st=2.54%20pin" H 7025 1400 50  0001 C CNN
F 4 "2984553" H 7025 1400 50  0001 C CNN "Farnell code"
F 5 "0.17" H 7025 1400 50  0001 C CNN "price"
	1    7025 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3875 2450 4175 2450
Wire Wire Line
	3875 2350 4175 2350
Wire Wire Line
	3875 2250 4175 2250
Text Label 3925 2350 0    50   ~ 0
MISO
Text Label 3925 2250 0    50   ~ 0
MOSI
Text Label 3925 2450 0    50   ~ 0
SCK
Text Label 7325 1400 0    50   ~ 0
MOSI
Text Label 6600 1300 0    50   ~ 0
MISO
Text Label 6600 1400 0    50   ~ 0
SCK
Wire Wire Line
	7325 1400 7500 1400
Wire Wire Line
	7325 1500 7500 1500
Wire Wire Line
	6825 1400 6600 1400
Wire Wire Line
	6825 1300 6600 1300
Text Label 6600 1500 0    50   ~ 0
RST
$Comp
L power:GND #PWR014
U 1 1 5F2F1F33
P 7500 1600
F 0 "#PWR014" H 7500 1350 50  0001 C CNN
F 1 "GND" H 7505 1427 50  0000 C CNN
F 2 "" H 7500 1600 50  0001 C CNN
F 3 "" H 7500 1600 50  0001 C CNN
	1    7500 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 1500 7500 1600
$Comp
L power:+3.3V #PWR01
U 1 1 5F2F3C71
P 2600 1875
F 0 "#PWR01" H 2600 1725 50  0001 C CNN
F 1 "+3.3V" H 2615 2048 50  0000 C CNN
F 2 "" H 2600 1875 50  0001 C CNN
F 3 "" H 2600 1875 50  0001 C CNN
	1    2600 1875
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 1875 2600 1950
Wire Wire Line
	2600 1950 2675 1950
Text Label 3975 3450 0    50   ~ 0
RST
Wire Wire Line
	3875 3450 4175 3450
Wire Wire Line
	3875 3650 4175 3650
Text Label 3975 3650 0    50   ~ 0
LED1
Wire Wire Line
	3875 3750 4175 3750
Text Label 3975 3750 0    50   ~ 0
LED2
Wire Wire Line
	3875 3850 4175 3850
Text Label 3975 3850 0    50   ~ 0
LED3
Wire Wire Line
	3875 3950 4175 3950
Text Label 3975 3950 0    50   ~ 0
LED4
Wire Wire Line
	3875 4050 4175 4050
Text Label 3975 4050 0    50   ~ 0
LED5
Wire Wire Line
	3875 4150 4175 4150
Text Label 3975 4150 0    50   ~ 0
LED6
Wire Wire Line
	3875 4250 4175 4250
Text Label 3975 4250 0    50   ~ 0
LED7
Wire Wire Line
	3875 4350 4175 4350
Text Label 3975 4350 0    50   ~ 0
LED8
Wire Wire Line
	9000 3000 9300 3000
Text Label 9100 3000 0    50   ~ 0
LED1
Wire Wire Line
	9000 3375 9300 3375
Text Label 9100 3375 0    50   ~ 0
LED2
Wire Wire Line
	9000 3750 9300 3750
Text Label 9100 3750 0    50   ~ 0
LED3
Wire Wire Line
	9000 4100 9300 4100
Text Label 9100 4100 0    50   ~ 0
LED4
Wire Wire Line
	9000 4450 9300 4450
Text Label 9100 4450 0    50   ~ 0
LED5
Wire Wire Line
	9000 4800 9300 4800
Text Label 9100 4800 0    50   ~ 0
LED6
Wire Wire Line
	9000 5150 9300 5150
Text Label 9100 5150 0    50   ~ 0
LED7
Wire Wire Line
	9000 5500 9300 5500
Text Label 9100 5500 0    50   ~ 0
LED8
$Comp
L Device:LED D2
U 1 1 5F303F44
P 9450 3375
F 0 "D2" H 9443 3120 50  0000 C CNN
F 1 "LED" H 9443 3211 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm_Clear" H 9450 3375 50  0001 C CNN
F 3 "~" H 9450 3375 50  0001 C CNN
F 4 "ADD in excel" H 9450 3375 50  0001 C CNN "Farnell code"
	1    9450 3375
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R2
U 1 1 5F303F4A
P 9800 3375
F 0 "R2" V 9604 3375 50  0000 C CNN
F 1 "100R" V 9695 3375 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 9800 3375 50  0001 C CNN
F 3 "~" H 9800 3375 50  0001 C CNN
F 4 "Add in excel" H 9800 3375 50  0001 C CNN "Farnell code"
	1    9800 3375
	0    1    1    0   
$EndComp
Wire Wire Line
	9600 3375 9700 3375
Wire Wire Line
	9900 3375 10000 3375
$Comp
L Device:LED D3
U 1 1 5F3053BB
P 9450 3750
F 0 "D3" H 9443 3495 50  0000 C CNN
F 1 "LED" H 9443 3586 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm_Clear" H 9450 3750 50  0001 C CNN
F 3 "~" H 9450 3750 50  0001 C CNN
F 4 "ADD in excel" H 9450 3750 50  0001 C CNN "Farnell code"
	1    9450 3750
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R3
U 1 1 5F3053C1
P 9800 3750
F 0 "R3" V 9604 3750 50  0000 C CNN
F 1 "100R" V 9695 3750 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 9800 3750 50  0001 C CNN
F 3 "~" H 9800 3750 50  0001 C CNN
F 4 "Add in excel" H 9800 3750 50  0001 C CNN "Farnell code"
	1    9800 3750
	0    1    1    0   
$EndComp
Wire Wire Line
	9600 3750 9700 3750
Wire Wire Line
	9900 3750 10000 3750
$Comp
L Device:LED D4
U 1 1 5F3067BB
P 9450 4100
F 0 "D4" H 9443 3845 50  0000 C CNN
F 1 "LED" H 9443 3936 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm_Clear" H 9450 4100 50  0001 C CNN
F 3 "~" H 9450 4100 50  0001 C CNN
F 4 "ADD in excel" H 9450 4100 50  0001 C CNN "Farnell code"
	1    9450 4100
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R4
U 1 1 5F3067C1
P 9800 4100
F 0 "R4" V 9604 4100 50  0000 C CNN
F 1 "100R" V 9695 4100 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 9800 4100 50  0001 C CNN
F 3 "~" H 9800 4100 50  0001 C CNN
F 4 "Add in excel" H 9800 4100 50  0001 C CNN "Farnell code"
	1    9800 4100
	0    1    1    0   
$EndComp
Wire Wire Line
	9600 4100 9700 4100
Wire Wire Line
	9900 4100 10000 4100
$Comp
L Device:LED D5
U 1 1 5F307B23
P 9450 4450
F 0 "D5" H 9443 4195 50  0000 C CNN
F 1 "LED" H 9443 4286 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm_Clear" H 9450 4450 50  0001 C CNN
F 3 "~" H 9450 4450 50  0001 C CNN
F 4 "ADD in excel" H 9450 4450 50  0001 C CNN "Farnell code"
	1    9450 4450
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R5
U 1 1 5F307B29
P 9800 4450
F 0 "R5" V 9604 4450 50  0000 C CNN
F 1 "100R" V 9695 4450 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 9800 4450 50  0001 C CNN
F 3 "~" H 9800 4450 50  0001 C CNN
F 4 "Add in excel" H 9800 4450 50  0001 C CNN "Farnell code"
	1    9800 4450
	0    1    1    0   
$EndComp
Wire Wire Line
	9600 4450 9700 4450
Wire Wire Line
	9900 4450 10000 4450
$Comp
L Device:LED D6
U 1 1 5F309352
P 9450 4800
F 0 "D6" H 9443 4545 50  0000 C CNN
F 1 "LED" H 9443 4636 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm_Clear" H 9450 4800 50  0001 C CNN
F 3 "~" H 9450 4800 50  0001 C CNN
F 4 "ADD in excel" H 9450 4800 50  0001 C CNN "Farnell code"
	1    9450 4800
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R6
U 1 1 5F309358
P 9800 4800
F 0 "R6" V 9604 4800 50  0000 C CNN
F 1 "100R" V 9695 4800 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 9800 4800 50  0001 C CNN
F 3 "~" H 9800 4800 50  0001 C CNN
F 4 "Add in excel" H 9800 4800 50  0001 C CNN "Farnell code"
	1    9800 4800
	0    1    1    0   
$EndComp
Wire Wire Line
	9600 4800 9700 4800
Wire Wire Line
	9900 4800 10000 4800
$Comp
L Device:LED D7
U 1 1 5F30C6CE
P 9450 5150
F 0 "D7" H 9443 4895 50  0000 C CNN
F 1 "LED" H 9443 4986 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm_Clear" H 9450 5150 50  0001 C CNN
F 3 "~" H 9450 5150 50  0001 C CNN
F 4 "ADD in excel" H 9450 5150 50  0001 C CNN "Farnell code"
	1    9450 5150
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R7
U 1 1 5F30C6D4
P 9800 5150
F 0 "R7" V 9604 5150 50  0000 C CNN
F 1 "100R" V 9695 5150 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 9800 5150 50  0001 C CNN
F 3 "~" H 9800 5150 50  0001 C CNN
F 4 "Add in excel" H 9800 5150 50  0001 C CNN "Farnell code"
	1    9800 5150
	0    1    1    0   
$EndComp
Wire Wire Line
	9600 5150 9700 5150
Wire Wire Line
	9900 5150 10000 5150
$Comp
L Device:LED D8
U 1 1 5F30E3C0
P 9450 5500
F 0 "D8" H 9443 5245 50  0000 C CNN
F 1 "LED" H 9443 5336 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm_Clear" H 9450 5500 50  0001 C CNN
F 3 "~" H 9450 5500 50  0001 C CNN
F 4 "ADD in excel" H 9450 5500 50  0001 C CNN "Farnell code"
	1    9450 5500
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R8
U 1 1 5F30E3C6
P 9800 5500
F 0 "R8" V 9604 5500 50  0000 C CNN
F 1 "100R" V 9695 5500 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 9800 5500 50  0001 C CNN
F 3 "~" H 9800 5500 50  0001 C CNN
F 4 "Add in excel" H 9800 5500 50  0001 C CNN "Farnell code"
	1    9800 5500
	0    1    1    0   
$EndComp
Wire Wire Line
	9600 5500 9700 5500
Wire Wire Line
	9900 5500 10000 5500
Wire Wire Line
	10000 3000 10000 3375
Connection ~ 10000 3375
Wire Wire Line
	10000 3375 10000 3750
Connection ~ 10000 3750
Wire Wire Line
	10000 3750 10000 4100
Connection ~ 10000 4100
Wire Wire Line
	10000 4100 10000 4450
Connection ~ 10000 4450
Wire Wire Line
	10000 4450 10000 4800
Connection ~ 10000 4800
Wire Wire Line
	10000 4800 10000 5150
Connection ~ 10000 5150
Wire Wire Line
	10000 5150 10000 5500
Connection ~ 10000 5500
$Comp
L power:GND #PWR016
U 1 1 5F31AAA3
P 10000 5950
F 0 "#PWR016" H 10000 5700 50  0001 C CNN
F 1 "GND" H 10005 5777 50  0000 C CNN
F 2 "" H 10000 5950 50  0001 C CNN
F 3 "" H 10000 5950 50  0001 C CNN
	1    10000 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3875 2850 4175 2850
Text Label 3975 2850 0    50   ~ 0
LED9
$Comp
L Device:R_POT RV1
U 1 1 5F31C6FB
P 7150 2575
F 0 "RV1" H 7080 2529 50  0000 R CNN
F 1 "R_POT" H 7080 2620 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_Piher_PT-10-H01_Horizontal" H 7150 2575 50  0001 C CNN
F 3 "https://lv.farnell.com/amphenol-piher-sensorscontrols/pt6kh-103a2020-pm/trimmer-10k-0-1w-1turn/dp/3128562?st=piher%20PT%206" H 7150 2575 50  0001 C CNN
F 4 "3128562" H 7150 2575 50  0001 C CNN "Farnell code"
F 5 "0.78" H 7150 2575 50  0001 C CNN "price"
	1    7150 2575
	-1   0    0    1   
$EndComp
Wire Wire Line
	3875 2950 4175 2950
Text Label 3975 2950 0    50   ~ 0
POT1
Wire Wire Line
	9025 5850 9325 5850
Text Label 9125 5850 0    50   ~ 0
LED9
$Comp
L Device:LED D9
U 1 1 5F3206CE
P 9475 5850
F 0 "D9" H 9468 5595 50  0000 C CNN
F 1 "LED" H 9468 5686 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm_Clear" H 9475 5850 50  0001 C CNN
F 3 "~" H 9475 5850 50  0001 C CNN
F 4 "ADD in excel" H 9475 5850 50  0001 C CNN "Farnell code"
	1    9475 5850
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R9
U 1 1 5F3206D4
P 9825 5850
F 0 "R9" V 9629 5850 50  0000 C CNN
F 1 "100R" V 9720 5850 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 9825 5850 50  0001 C CNN
F 3 "~" H 9825 5850 50  0001 C CNN
F 4 "Add in excel" H 9825 5850 50  0001 C CNN "Farnell code"
	1    9825 5850
	0    1    1    0   
$EndComp
Wire Wire Line
	9625 5850 9725 5850
Wire Wire Line
	10000 5500 10000 5850
Wire Wire Line
	9925 5850 10000 5850
Connection ~ 10000 5850
Wire Wire Line
	10000 5850 10000 5950
Text Label 6725 2575 0    50   ~ 0
POT1
Wire Wire Line
	6725 2575 7000 2575
$Comp
L power:GND #PWR012
U 1 1 5F32F649
P 7150 2725
F 0 "#PWR012" H 7150 2475 50  0001 C CNN
F 1 "GND" H 7155 2552 50  0000 C CNN
F 2 "" H 7150 2725 50  0001 C CNN
F 3 "" H 7150 2725 50  0001 C CNN
	1    7150 2725
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 5F32FBA1
P 3275 4650
F 0 "#PWR03" H 3275 4400 50  0001 C CNN
F 1 "GND" H 3280 4477 50  0000 C CNN
F 2 "" H 3275 4650 50  0001 C CNN
F 3 "" H 3275 4650 50  0001 C CNN
	1    3275 4650
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR011
U 1 1 5F330031
P 7150 2425
F 0 "#PWR011" H 7150 2275 50  0001 C CNN
F 1 "+3.3V" H 7165 2598 50  0000 C CNN
F 2 "" H 7150 2425 50  0001 C CNN
F 3 "" H 7150 2425 50  0001 C CNN
	1    7150 2425
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW2
U 1 1 5F330E81
P 9425 1650
F 0 "SW2" H 9425 1935 50  0000 C CNN
F 1 "SW_Push" H 9425 1844 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 9425 1850 50  0001 C CNN
F 3 "https://lv.farnell.com/alcoswitch-te-connectivity/1825910-7/tactile-switch-spst-0-05a-24v/dp/2468761?st=tact%20switch" H 9425 1850 50  0001 C CNN
F 4 "2469761" H 9425 1650 50  0001 C CNN "Farnell code"
F 5 "0.07" H 9425 1650 50  0001 C CNN "price"
	1    9425 1650
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW3
U 1 1 5F332A4D
P 9425 2050
F 0 "SW3" H 9425 2335 50  0000 C CNN
F 1 "SW_Push" H 9425 2244 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 9425 2250 50  0001 C CNN
F 3 "https://lv.farnell.com/alcoswitch-te-connectivity/1825910-7/tactile-switch-spst-0-05a-24v/dp/2468761?st=tact%20switch" H 9425 2250 50  0001 C CNN
F 4 "2469761" H 9425 2050 50  0001 C CNN "Farnell code"
F 5 "0.07" H 9425 2050 50  0001 C CNN "price"
	1    9425 2050
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR015
U 1 1 5F334D7B
P 9700 1525
F 0 "#PWR015" H 9700 1375 50  0001 C CNN
F 1 "+3.3V" H 9715 1698 50  0000 C CNN
F 2 "" H 9700 1525 50  0001 C CNN
F 3 "" H 9700 1525 50  0001 C CNN
	1    9700 1525
	1    0    0    -1  
$EndComp
Wire Wire Line
	9700 1525 9700 1650
Wire Wire Line
	9700 2050 9625 2050
Wire Wire Line
	9625 1650 9700 1650
Connection ~ 9700 1650
Wire Wire Line
	9700 1650 9700 2050
Wire Wire Line
	9025 1650 9225 1650
Wire Wire Line
	9225 2050 9025 2050
Text Label 9025 1650 0    50   ~ 0
SW1
Text Label 9025 2050 0    50   ~ 0
SW2
$Comp
L Device:Battery_Cell BT1
U 1 1 5F344F77
P 3775 6150
F 0 "BT1" V 3525 6050 50  0000 C CNN
F 1 "Battery_Cell" V 3625 6075 50  0000 C CNN
F 2 "Battery:BatteryHolder_Keystone_3002_1x2032" V 3775 6210 50  0001 C CNN
F 3 "https://lv.farnell.com/keystone/3002/battery-smd-retainer-20mm/dp/1650693?st=keystone%203002" V 3775 6210 50  0001 C CNN
F 4 "1650693" H 3775 6150 50  0001 C CNN "Farnell code"
F 5 "0.87" H 3775 6150 50  0001 C CNN "price"
	1    3775 6150
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR05
U 1 1 5F346DE2
P 3600 6225
F 0 "#PWR05" H 3600 5975 50  0001 C CNN
F 1 "GND" H 3605 6052 50  0000 C CNN
F 2 "" H 3600 6225 50  0001 C CNN
F 3 "" H 3600 6225 50  0001 C CNN
	1    3600 6225
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 6225 3600 6150
Wire Wire Line
	3600 6150 3675 6150
Wire Wire Line
	4800 6050 5000 6050
Wire Wire Line
	5000 6050 5000 5950
$Comp
L power:+3.3V #PWR06
U 1 1 5F34BD06
P 5000 5950
F 0 "#PWR06" H 5000 5800 50  0001 C CNN
F 1 "+3.3V" H 5015 6123 50  0000 C CNN
F 2 "" H 5000 5950 50  0001 C CNN
F 3 "" H 5000 5950 50  0001 C CNN
	1    5000 5950
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_SPDT SW1
U 1 1 5F34C9C2
P 4600 6050
F 0 "SW1" H 4600 6335 50  0000 C CNN
F 1 "SW_SPDT" H 4600 6244 50  0000 C CNN
F 2 "Button_Switch_THT:SW_CuK_OS102011MA1QN1_SPDT_Angled" H 4600 6050 50  0001 C CNN
F 3 "https://lv.farnell.com/c-k-components/os102011ma1qn1/switch-spdt-0-1a-12v-pcb-r-a/dp/1201431?st=OS102011MA1QN1" H 4600 6050 50  0001 C CNN
F 4 "1201431" H 4600 6050 50  0001 C CNN "Farnell code"
F 5 "0.39" H 4600 6050 50  0001 C CNN "price"
	1    4600 6050
	-1   0    0    1   
$EndComp
Wire Wire Line
	3875 3050 4175 3050
Text Label 3975 3050 0    50   ~ 0
SW1
Text Label 3975 3150 0    50   ~ 0
SW2
Wire Wire Line
	3875 3350 4175 3350
$Comp
L Connector:TestPoint TP1
U 1 1 5F360928
P 4175 3350
F 0 "TP1" V 4129 3538 50  0000 L CNN
F 1 "TestPoint" V 4220 3538 50  0000 L CNN
F 2 "TestPoint:TestPoint_Loop_D2.50mm_Drill1.0mm" H 4375 3350 50  0001 C CNN
F 3 "~" H 4375 3350 50  0001 C CNN
	1    4175 3350
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP2
U 1 1 5F361555
P 4500 3250
F 0 "TP2" V 4454 3438 50  0000 L CNN
F 1 "TestPoint" V 4545 3438 50  0000 L CNN
F 2 "TestPoint:TestPoint_Loop_D2.50mm_Drill1.0mm" H 4700 3250 50  0001 C CNN
F 3 "~" H 4700 3250 50  0001 C CNN
	1    4500 3250
	0    1    1    0   
$EndComp
Wire Wire Line
	3875 3150 4175 3150
Wire Wire Line
	3875 3250 4500 3250
$Comp
L Device:C_Small C1
U 1 1 5F2D98F2
P 5275 6650
F 0 "C1" V 5046 6650 50  0000 C CNN
F 1 "100N" V 5137 6650 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 5275 6650 50  0001 C CNN
F 3 "https://lv.farnell.com/avx/12061c104jat2a/cap-0-1-f-100v-5-x7r-1206/dp/2332842?ost=2332842" H 5275 6650 50  0001 C CNN
F 4 "2332842" H 5275 6650 50  0001 C CNN "Farnell code"
F 5 "0.32" H 5275 6650 50  0001 C CNN "price"
	1    5275 6650
	-1   0    0    1   
$EndComp
$Comp
L power:+3.3V #PWR07
U 1 1 5F2DA6D0
P 5275 6550
F 0 "#PWR07" H 5275 6400 50  0001 C CNN
F 1 "+3.3V" H 5290 6723 50  0000 C CNN
F 2 "" H 5275 6550 50  0001 C CNN
F 3 "" H 5275 6550 50  0001 C CNN
	1    5275 6550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR08
U 1 1 5F2DAB6C
P 5275 6750
F 0 "#PWR08" H 5275 6500 50  0001 C CNN
F 1 "GND" H 5280 6577 50  0000 C CNN
F 2 "" H 5275 6750 50  0001 C CNN
F 3 "" H 5275 6750 50  0001 C CNN
	1    5275 6750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 1500 6350 1325
$Comp
L Device:R_Small R10
U 1 1 5F2DDBD9
P 6350 1225
F 0 "R10" V 6154 1225 50  0000 C CNN
F 1 "4.7k" V 6245 1225 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 6350 1225 50  0001 C CNN
F 3 "https://lv.farnell.com/multicomp/mcre000045/res-4k7-5-125mw-axial-carbon-film/dp/1700245?st=4.7k" H 6350 1225 50  0001 C CNN
F 4 "1700245" H 6350 1225 50  0001 C CNN "Farnell code"
F 5 "0.02" H 6350 1225 50  0001 C CNN "price"
	1    6350 1225
	-1   0    0    1   
$EndComp
Wire Wire Line
	6350 1500 6825 1500
$Comp
L power:+3.3V #PWR010
U 1 1 5F2E1141
P 6350 1125
F 0 "#PWR010" H 6350 975 50  0001 C CNN
F 1 "+3.3V" H 6365 1298 50  0000 C CNN
F 2 "" H 6350 1125 50  0001 C CNN
F 3 "" H 6350 1125 50  0001 C CNN
	1    6350 1125
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H1
U 1 1 5F364780
P 6200 3625
F 0 "H1" H 6300 3671 50  0000 L CNN
F 1 "MountingHole" H 6300 3580 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 6200 3625 50  0001 C CNN
F 3 "~" H 6200 3625 50  0001 C CNN
	1    6200 3625
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5F367E3C
P 6925 3625
F 0 "H3" H 7025 3671 50  0000 L CNN
F 1 "MountingHole" H 7025 3580 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 6925 3625 50  0001 C CNN
F 3 "~" H 6925 3625 50  0001 C CNN
	1    6925 3625
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5F3681BE
P 6200 4025
F 0 "H2" H 6300 4071 50  0000 L CNN
F 1 "MountingHole" H 6300 3980 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 6200 4025 50  0001 C CNN
F 3 "~" H 6200 4025 50  0001 C CNN
	1    6200 4025
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5F368BA0
P 6925 4025
F 0 "H4" H 7025 4071 50  0000 L CNN
F 1 "MountingHole" H 7025 3980 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 6925 4025 50  0001 C CNN
F 3 "~" H 6925 4025 50  0001 C CNN
	1    6925 4025
	1    0    0    -1  
$EndComp
Wire Wire Line
	7325 1300 7800 1300
Text Label 7575 1300 0    50   ~ 0
3Vfeed
Text Label 4075 5950 0    50   ~ 0
3Vfeed
Wire Wire Line
	3975 6150 4400 6150
Wire Wire Line
	4400 5950 4075 5950
$EndSCHEMATC
