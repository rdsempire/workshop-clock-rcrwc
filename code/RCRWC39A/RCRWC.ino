/*
Robocam workshop programmable clock
Code and PCB design by Raivis Deksnis 2020
case design by Jānis Lapsa 2020
Improvements by : insert your name here :D 
*/

unsigned long ledSpeed = 1000; //stores at what speed annimation should run
unsigned long millisStart = 0; //used for protothreading
unsigned long analogValue = 0; //value read from potentiometer


int ledCounter = 0; //counts LED position
int diodeState[9][2]; //row, collumn,  ; ID , mode



int buttonState1 = 0;  //left button state
int buttonState2 = 0;  //right button state
int deviceMode = 0; //which annimation device is running
#define Lit 0 //All segments lit
#define Scroll 1 //scroll segments, potentiometer value determines how many segments are lit.
#define Clock 2 //Regular clock
#define Stacker 3 //Stacker game unfinished
#define Custom1 4 //place your own annimation/game here
#define Custom2 5 //place your own annimatio/game here

int unpressChecker = 0; //used to determine if botton has been unpressed at least once.

//
int ledAnnimationCounter = 0; //used for rotational LED annimations, to determine current position

//scroll annimation variables
#define scrollAnimationSpeed 250 //annimation speed in milliseconds for scroll annimation
int traveledDistance = 0; //how far bar graph has gone
int ledAnimationDirection = 1; // 1 =  forwards 2 = backwards

//clock variables
#define clockSecondLenght 995 //lenght of one second for clock, used to calibrate time (instructions take time too)

//stacker variables
int stackerActionState = 0; //which mode stacker is on
int stackerStartingPoint = 0;


// the setup function runs once when you press reset or power the board
void setup() {

 
//define ID's
diodeState[0][0] = 0;
diodeState[1][0] = 1;
diodeState[2][0] = 2;
diodeState[3][0] = 3;
diodeState[4][0] = 4;
diodeState[5][0] = 5;
diodeState[6][0] = 6;
diodeState[7][0] = 7;
diodeState[8][0] = 14;

//define default pin modes
diodeState[0][1] = 0;
diodeState[1][1] = 0;
diodeState[2][1] = 0;
diodeState[3][1] = 0;
diodeState[4][1] = 0;
diodeState[5][1] = 0;
diodeState[6][1] = 0;
diodeState[7][1] = 0;
diodeState[8][1] = 0;



  
  // initialize digital pin LED_BUILTIN as an output.
  /*
  pinMode(A0, OUTPUT);
  for(int i=0; i<8; i++){
    pinMode(i, OUTPUT);
  }
  */

pinMode(0, OUTPUT);
pinMode(1, OUTPUT);
pinMode(2, OUTPUT);
pinMode(3, OUTPUT);
pinMode(4, OUTPUT);
pinMode(5, OUTPUT);
pinMode(6, OUTPUT);
pinMode(7, OUTPUT);
pinMode(14, OUTPUT);

/*
  digitalWrite(diodeState[0][1], LOW);
  digitalWrite(diodeState[1][1], LOW);
  digitalWrite(diodeState[2][1], LOW);
  digitalWrite(diodeState[3][1], LOW);
  digitalWrite(diodeState[4][1], LOW);
  digitalWrite(diodeState[5][1], LOW);
  digitalWrite(diodeState[6][1], LOW);
  digitalWrite(diodeState[7][1], LOW);
  digitalWrite(diodeState[8][1], LOW);
  */
  //potentiometer
  pinMode(A1, INPUT);

  
pinMode(A2, INPUT); //bottons
pinMode(A3, INPUT);

  millisStart = millis();
  
}

// the loop function runs over and over again forever
void lit(){ //lit annimation sector change
  if(buttonState2 == 0){
    
diodeState[0][1] = 1;
diodeState[1][1] = 1;
diodeState[2][1] = 1;
diodeState[3][1] = 1;
diodeState[4][1] = 1;
diodeState[5][1] = 1;
diodeState[6][1] = 1;
diodeState[7][1] = 1;
diodeState[8][1] = 1;

  }
  else
  {
diodeState[0][1] = 0;
diodeState[1][1] = 0;
diodeState[2][1] = 0;
diodeState[3][1] = 0;
diodeState[4][1] = 0;
diodeState[5][1] = 0;
diodeState[6][1] = 0;
diodeState[7][1] = 0;
diodeState[8][1] = 0;
  }
}

void setDiodeStateZero(){ //sets diode state to unlit
diodeState[0][1] = 0;
diodeState[1][1] = 0;
diodeState[2][1] = 0;
diodeState[3][1] = 0;
diodeState[4][1] = 0;
diodeState[5][1] = 0;
diodeState[6][1] = 0;
diodeState[7][1] = 0;
diodeState[8][1] = 0;
}


void loop() {
  //first section, read all buttons, display LED state determined by ''diodeState[1][2]
ledSpeed = analogRead(A1);
buttonState1 = digitalRead(A3);
buttonState2 = digitalRead(A2);


    digitalWrite(diodeState[ledCounter][0], LOW);
    ledCounter++;
    //delay(10);
    
    if(ledCounter > 8){
    ledCounter = 0;
  }
  
    digitalWrite(diodeState[ledCounter][0], diodeState[ledCounter][1]);    // turn the LED off by making the voltage LOW
    //delay(1000);
  //digitalWrite(A0,LOW);
 // ledCounter++;

//annimation/game codes, 
switch(deviceMode){
//-----------LIT----------------  
      case Lit:
      lit();
      if(buttonState1 ==  1 && unpressChecker == 0 ){
        delay(100);
        buttonState1 = digitalRead(A3);
        if(buttonState1 == 1){
        deviceMode = Scroll;
        setDiodeStateZero();
        
        delay(100);
        }
      }
      else if (buttonState1 = 0 && unpressChecker == 1){
        delay(100);
        buttonState1 = digitalRead(A3);
        
        if(buttonState1 == 0){
        unpressChecker == 0;
        delay(100);
        }
      }
      break;
//---------SCROLL---------------      
      case Scroll:
      
      if((millis() - millisStart ) > ledSpeed)//scrollAnimationSpeed
      {
    millisStart = millis();
      ledAnnimationCounter++;
      traveledDistance++;
      
      if(ledAnnimationCounter > 7){ //since we have 8 leds, if counter goes above 7 set it to zero
        ledAnnimationCounter = 0;
       }

       
       if(traveledDistance > 8)
       {
        if(ledAnimationDirection == 0)
        {
          ledAnimationDirection = 1;
          ledAnnimationCounter--;
          if(ledAnnimationCounter == -1){ //this is safety net so we don't skip one segment
            ledAnnimationCounter = 7;
          }
        }
        else if(ledAnimationDirection == 1)
        {
          ledAnimationDirection = 0;
          
        }
        traveledDistance = 0;
        
        //ledAnnimationCounter
       }
        //annimation code
       if (ledAnimationDirection == 1){
        diodeState[ledAnnimationCounter][1] = 1;
        diodeState[8][1] = 1;
        
       }
       else if (ledAnimationDirection == 0) {
        diodeState[ledAnnimationCounter][1] = 0;
        diodeState[8][1] = 0;
       }
        
        

       
      }
      


      
    if(buttonState1 ==  1 && unpressChecker == 0){
       delay(100);
        buttonState1 = digitalRead(A3);
        if(buttonState1 == 1){
        deviceMode = Clock;
        //reset everything to default state before moving to next animation
        setDiodeStateZero();
        ledAnnimationCounter = 0;
        traveledDistance = 1;
        delay(100);
        }
      }
      else if (buttonState1 = 0 && unpressChecker == 1){
        delay(100);
        buttonState1 = digitalRead(A3);
        
        if(buttonState1 == 0){
        unpressChecker == 0;
        delay(100);
        }
      }
       break;
//-----------CLOCK--------------------------       
       case Clock:

    if((millis() - millisStart ) > clockSecondLenght)//clock speed
      {
    millisStart = millis();

diodeState[ledAnnimationCounter][1] = 0;
ledAnnimationCounter++;
if(ledAnnimationCounter > 7){
  ledAnnimationCounter = 0;
  }
        diodeState[ledAnnimationCounter][1] = 1;
        diodeState[8][1] = 1;
        
      }


       
      if(buttonState1 ==  1 && unpressChecker == 0){
       delay(100);
        buttonState1 = digitalRead(A3);
        if(buttonState1 == 1){
        deviceMode = Lit;
        setDiodeStateZero();
        ledAnnimationCounter = 0;
        delay(100);
        }
      }
      else if (buttonState1 = 0 && unpressChecker == 1){
        delay(100);
        buttonState1 = digitalRead(A3);
        
        if(buttonState1 == 0){
        unpressChecker == 0;
        delay(100);
        }
      }
       break;
//-----------STACKER-----------------------
      case Stacker:

      /*

if((millis() - millisStart ) > clockSecondLenght)//clock speed
      {
         millisStart = millis();
if(stackerActionState == 0){ //waiting for player input
        
   

diodeState[ledAnnimationCounter][1] = 0;
ledAnnimationCounter++;
if(ledAnnimationCounter > 7){
  ledAnnimationCounter = 0;
  }
        diodeState[ledAnnimationCounter][1] = 1;
        diodeState[8][1] = 0;



if(buttonState1 ==  1 && unpressChecker == 0){ //if button pressed, move on
       delay(100);
        buttonState1 = digitalRead(A3);
        if(buttonState1 == 1){
        deviceMode = Custom1;
        delay(100);
        }
      }
      else if (buttonState1 = 0 && unpressChecker == 1){
        delay(100);
        buttonState1 = digitalRead(A3);
        
        if(buttonState1 == 0){
        unpressChecker == 0;
        delay(100);
        }
      }


        
       }
else if(stackerActionState == 1){ //start of the game
       }




        
      }

      
*/

 if(buttonState1 ==  1 && unpressChecker == 0){
       delay(100);
        buttonState1 = digitalRead(A3);
        if(buttonState1 == 1){
        deviceMode = Custom1;
        delay(100);
        }
      }
      else if (buttonState1 = 0 && unpressChecker == 1){
        delay(100);
        buttonState1 = digitalRead(A3);
        
        if(buttonState1 == 0){
        unpressChecker == 0;
        delay(100);
        }
      }
    
       break;
  
       
//------------CUSTOM_1-------------------------------
       case Custom1:
      if(buttonState1 ==  1 && unpressChecker == 0){
       delay(100);
        buttonState1 = digitalRead(A3);
        if(buttonState1 == 1){
        deviceMode = Custom2;
        delay(100);
        }
      }
      else if (buttonState1 = 0 && unpressChecker == 1){
        delay(100);
        buttonState1 = digitalRead(A3);
        
        if(buttonState1 == 0){
        unpressChecker == 0;
        delay(100);
        }
      }
      break;
//-----------CUSTOM_2-------------------------------------
       case Custom2:
       
      if(buttonState1 ==  1 && unpressChecker == 0){
       delay(100);
        buttonState1 = digitalRead(A3);
        if(buttonState1 == 1){
        deviceMode = Lit;
        delay(100);
        }
      }
      else if (buttonState1 = 0 && unpressChecker == 1){
        delay(100);
        buttonState1 = digitalRead(A3);
        
        if(buttonState1 == 0){
        unpressChecker == 0;
        delay(100);
        }
      }
      break;
      //default:
  }
}
